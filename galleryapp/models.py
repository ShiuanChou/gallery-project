from asyncio.windows_events import NULL
from django.db import models

# Create your models here.
class AlbumModel(models.Model):
    adate = models.DateTimeField(auto_now=True) #相簿建立日期
    atitle = models.CharField(max_length=100, null=False) #相簿標題
    auploadname=models.CharField(max_length=100, null=False,default="")  #相簿上傳者
    def str(self):
        return self.atitle  #在admin回傳相簿標題

class PhotoModel(models.Model):
   palbum = models.ForeignKey('AlbumModel', on_delete = models.SET_NULL, null = True,blank=True) #表示刪除AlbumModel的相簿時內容會自動刪掉
   puploadname=models.CharField(max_length=100, null=False,default="") #照片上傳者
   pdate = models.DateTimeField(auto_now=True)  #相片建立日期
   purl = models.CharField(max_length=100, null=False)  #相片檔案路徑
   ptrash = models.IntegerField (default=0)   #垃圾桶
   pfavorite = models.IntegerField (default=0)  #最愛

   def str(self):
        return self.purl   #在admin回傳相片標題